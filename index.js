
const fastify = require('fastify')()
    cors = require('@fastify/cors');

fastify.register(cors, {
    origin: "*",
});

function isValidHttpUrl(string) {
    let url;
    try {
      url = new URL(string);
    } catch (_) {
      return false;
    }
    return true;
}

const v1Routes = require('./routes/user')
v1Routes.forEach((route, index) => {
    fastify.route(route)
})

fastify.listen({ port: 3000 }, function (err, address) {
    if (err) {
        fastify.log.error(err)
        process.exit(1)
    }
    // Server is now listening on ${address}
    console.log("Server is listening at http://localhost:3000")
})