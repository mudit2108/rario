const userRequestValidation = {
    type: 'object',
    required: ['email', 'password', 'name'],
    properties: {
        someKey: { type: 'string' },
        someOtherKey: { type: 'number' },
        requiredKey: {
        type: 'array',
        maxItems: 3,
        items: { type: 'integer' }
    },
    nullableKey: { type: ['number', 'null'] }, // or { type: 'number', nullable: true }
    multipleTypesKey: { type: ['boolean', 'number'] },
    multipleRestrictedTypesKey: {
    oneOf: [
        { type: 'string', maxLength: 5 },
        { type: 'number', minimum: 10 }
    ]
    },
    enumKey: {
    type: 'string',
    enum: ['John', 'Foo']
    },
    notTypeKey: {
    not: { type: 'array' }
    }
    }
}

module.exports = {userRequestValidation};