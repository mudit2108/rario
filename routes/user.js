const userController = require("../controllers/users");

const routes = [{
    method: 'GET',
    url: '/user',
    handler: userController.getAllUsers
},
{
    method: 'POST',
    url: '/user',
    handler: userController.createUser
}]

module.exports = routes;