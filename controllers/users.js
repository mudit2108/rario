const userService = require("../services/users");

let users = [];

function isNumber(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
  }

const getAllUsers = async (req, reply) => {
    let page = 1;
    let size = 2;
    if (req.query.page && isNumber(req.query.page)){
        page = parseInt(req.query.page);
    }

    if (req.query.size && isNumber(req.query.size)){
        page = parseInt(req.query.size);
    }
    
    reply.send(userService.getAllUsers(page, size));
}

const createUser = async (req, reply) => {
    let newUser = userService.addNewUser(req.body);
    reply.send(newUser);
}

module.exports = {
    getAllUsers,
    createUser
}