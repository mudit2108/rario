let users = [];

const getUser = (pk) => {
    if (pk < users.length+1) {
        return {
            id: users[pk].id,
            name: users[pk].name,
            email: users[pk].email
        }
    }else{
        return {}
    }
    
}

const addNewUser = (data) => {
    let userData = {
        id: users.length+1,
        name: data.name,
        email: data.email,
        password: data.password
    }
    users.push(userData);
    return getUser(users.length-1);
}

const getAllUsers = (page, size) => {
    let response = [];
    let userSubArray = users.slice((page-1)*size, page*size);
    console.log(userSubArray);
    userSubArray.forEach((element, i) => {
        response.push(getUser(i));
    });
    return response;
}

module.exports = {
    addNewUser,
    getAllUsers
}